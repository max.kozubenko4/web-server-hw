const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');

router.get(/(\/api\/files\/)\w+\.?\w+\.?(log|txt|json|yaml|xml|js)?$/, function (req, res) {
    const filePath = req.url.slice(req.url.lastIndexOf('/') + 1);
    const mainDir = './files/';

    console.log(filePath);

    const file = {
        message: "Success",
        filename: filePath,
        content: "",
        extension: path.extname(filePath).slice(1),
        uploadedDate: ""
    };

    const fileFailture = {
        message: "Success"
    }

    if (fs.existsSync(mainDir + filePath)) {
        const { birthtime } = fs.statSync(mainDir + filePath);
        file.uploadedDate = birthtime;
        fs.readFile(mainDir + filePath, 'utf8', (error,data) => {
            if(error) {
                fileFailture.message = `Server error`;
                res.status(500).json(file);
            };
            file.content = data;
            console.log(file);
            res.status(200).json(file);
        });
    } else {
        fileFailture.message = `No file with ${filePath} filename found`;
        res.status(400).json(fileFailture);
    }
});

module.exports = { 
    getFile: router, 
}
