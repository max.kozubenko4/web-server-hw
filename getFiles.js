const express = require('express');
const router = express.Router();

const fs = require('fs');

router.get('/api/files', function (req, res) {
    const fileArr = {
        message: "Success",
        files: []
    };

    const fileFailture = {
        message: "",
    };

    const mainDir = './files/';

    if (fs.existsSync(mainDir)) {
        fs.readdir('./files/', (err, files) => {
            if(err) {
                fileFailture.message(`Server error`);
                throw res.status(500).json(fileFailture);
            }

            files.forEach(file => {
                fileArr.files.push(file);
            });
    
            if(files.length === 0) {
                fileFailture.message = `Client error`;
                res.status(400).json(fileFailture);
            } else {
                res.status(200).json(fileArr);
            }
        });
    } else {
        fileFailture.message = `Client error`;
        res.status(400).json(fileFailture);
    }    
});

module.exports = { 
    getFiles: router, 
}
