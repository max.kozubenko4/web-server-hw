//Reserved modules
const express = require('express');
const app = express();
const morgan = require('morgan');
const fs = require('fs');

//Custom modules
const { createFile } = require('./createFile');
const { getFiles } = require('./getFiles');
const { getFile } = require('./getFile');
// const { deleteFile } = require('./deleteFile');
// const { changeFile } = require('./changeFile');


app.use(express.json());
app.use(morgan('tiny'));

app.use(createFile);
app.use(getFiles);
app.use(getFile);
// app.use(deleteFile);
// app.use(changeFile);

app.listen(8080);