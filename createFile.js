const express = require('express');
const router = express.Router();
const fs = require('fs');

router.post('/api/files', function (req, res) {
    const { filename, content } = req.body;

    const responceObj = {
        message: '',
    }

    if(!content || filename == undefined) {
        responceObj.message = `Please specify 'content' parameter`;
        res.status(400).json(responceObj);
    }

    if(/\w+\.(log|txt|json|yaml|xml|js)+$/i.test(filename)) {
        fs.writeFile('./files/' + filename, content, (err) => {
            if(err) {
                responceObj.message = `Server error`;
                throw res.status(500).json(responceObj);
            }
        });
    }
    
    responceObj.message = `File created successfully`;
    res.status(200).json(responceObj);
});

module.exports = { 
    createFile: router, 
}
